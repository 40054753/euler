﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Euler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int largestPalindrome = 0;
        public MainWindow()
        {
            InitializeComponent();
            palindrome();
            palindromeTB.Text = largestPalindrome.ToString(); //convert largest palindrome to string and print on window
        }
        public void palindrome()
        {
            for (int a = 316; a < 1000; a++)
            {
                for (int b = 316; b < 1000; b++)
                {
                    int palindromeNumber = a * b; //calculate the potential palindrome
                    string palinString = palindromeNumber.ToString();
                    int int0 = 0;
                    int int5 = palinString.Length - 1;
                    bool palin = true;
                    while (palin) //check for the palindrome
                    {
                        char c = palinString[int0];
                        char d = palinString[int5];

                        if (c != d) //if two characters are different it is not a palindrome
                        {
                            palin = false;
                        }

                        if (int0 > int5) // if we have not found a different character by this point, it must be a palindrome
                        {
                            if (palindromeNumber > largestPalindrome) //if this palindrome is larger than the last
                            {
                                largestPalindrome = palindromeNumber;
                            }
                            palin = false;
                        }
                        int0++;
                        int5--;
                        }
                    }
                  }
                {
            }
        }
    }
}
